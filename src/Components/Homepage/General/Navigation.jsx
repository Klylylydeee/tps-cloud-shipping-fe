// NPM Packages
import React, { useState } from "react";
import { Link } from "react-router-dom";
// Assets
import Logo from "src/Assets/logo.png";
import MenuIcon from 'src/Assets/bars-solid.svg'
import CloseIcon from 'src/Assets/window-close-regular.svg'

const Navigation = () => {
    //We will use react hooks for toggling the menu
    const [isSideMenuOpen, setisSideMenuOpen] = useState(false)

    const showSideMenu = () => {
        (isSideMenuOpen) ? setisSideMenuOpen(false) : setisSideMenuOpen(true)
    }
    return (
        <nav className="relative z-10 bg-white">
            <div className="flex justify-between max-w-7xl mx-auto">
                <Link to="/">
                    <div className="flex items-center py-2.5">
                        <img src={Logo} alt="" className="brand-logo w-24" />
                        <div className="brand-name ml-3 text-2xl font-bold text-tiffany">Cloud Shipping</div>
                    </div>
                </Link>
                <ul className="hidden menu-list items-center lg:flex lg:flex-row">
                    <li className="menu-list-items ">
                        <Link to="/" className="text-lg my-1 text-tiffany dark:text-gray-200 hover:text-indigo-600 md:mx-4 md:my-0">
                            Home
                        </Link>
                    </li>
                    <li className="menu-list-items" >
                        <Link to="/services" className="text-lg my-1 text-tiffany  dark:text-gray-200 hover:text-indigo-600  md:mx-4 md:my-0">
                            Service
                        </Link>
                    </li>
                    <li className="menu-list-items ">
                        <Link to="/contact" className="text-lg my-1 text-tiffany  dark:text-gray-200 hover:text-indigo-600 md:mx-4 md:my-0">
                            Contact
                        </Link>
                    </li>
                    <li className="menu-list-items">
                        <Link to="/team" className="text-lg my-1 text-tiffany  dark:text-gray-200 hover:text-indigo-600  md:mx-4 md:my-0">
                            Team
                        </Link>
                    </li>
                    <li className="menu-list-items ">
                        <Link to="/become-a-partner" className="text-lg my-1 text-tiffany  dark:text-gray-200 hover:text-indigo-600  md:mx-4 md:my-0">
                            Partner
                        </Link>
                    </li>
                </ul>
                <button onClick={() => { showSideMenu() }} className="lg:hidden menu-button ">
                    {(isSideMenuOpen) ? <img src={CloseIcon} className="w-9 h-9 px-2 text-tiffany hover:bg-tiffany transition duration-500" alt="close"></img> : <img src={MenuIcon} className="w-9 h-9 px-2  hover:bg-tiffany transition duration-500" alt="menu"></img>}
                </button>
                {(isSideMenuOpen) ? SideMenu() : ''}
            </div>
        </nav>
    )
}
function SideMenu() {
    return (
        <div className="fixed w-full sm:w-1/4 lg:hidden top-14 bg-white">
            <Link to="/" className="block py-2 px-4 hover:bg-gray-100 my-1 text-tiffany">Home</Link>
            <Link to="/services" className="block py-2 px-4 hover:bg-gray-100 my-1 text-tiffany">Service</Link>
            <Link to="/contact" className="block py-2 px-4 hover:bg-gray-100 my-1 text-tiffany">Contact</Link>
            <Link to="/team" className="block py-2 px-4 hover:bg-gray-100 my-1 text-tiffany">Team</Link>
            <Link to="/become-a-partner" className="block py-2 px-4 hover:bg-gray-100 my-1 text-tiffany">Partner</Link>
        </div>
    )
}
export default Navigation;
