// NPM Packages
import React from "react";

// Components
import Navigation from "src/Components/Homepage/General/Navigation";
import Footer from "src/Components/Homepage/General/Footer";

import Blog from "src/Components/Homepage/Landing/Blog";
import Hero from "src/Components/Homepage/Landing/Hero";
import Review from "src/Components/Homepage/Landing/Review";
import Services from "src/Components/Homepage/Landing/Services";
import Workflow from "src/Components/Homepage/Landing/Workflow";

const Landing = () => {
    return (
        <div>
            <Navigation />
            <Hero />
            <Services />
            <Workflow />
            <Review />
            <Blog />
            <Footer />
        </div>
    )
}

export default Landing
