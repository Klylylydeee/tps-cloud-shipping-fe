// NPM Packages
import React from "react"

// Components
import Navigation from "src/Components/Homepage/General/Navigation";
import Footer from "src/Components/Homepage/General/Footer";

const Contact = () => {
    return (
        <div className='antialiased bg-tiffany-light'>
            <Navigation />
            <div className='flex w-full min-h-screen justify-center items-center pt-20 pb-20'>
                <div
                    className='flex flex-col md:flex-row md:space-x-6 space-y-6 md:space-y-0 bg-dark-gray w-full max-w-4xl p-8 sm:p-12 rounded-xl shadow-lg text-white  overflow-hidden'>
                    <div className='flex flex-col space-y-8 justify-between text-cyan-100'>
                        <div>
                            <h1 className='font-bold text-tiffany text-4xl tracking-wide leading-relaxed'>Contact Us</h1>
                            <p className='pt-2  text-sm'>
                                For support please contact or message:
                            </p>
                        </div>
                        <div className='flex flex-col space-y-6 '>
                            <div className='inline-flex space-x-2 items-center  text-xl'>
                                <ion-icon
                                    name="call"
                                ></ion-icon>
                                <span>+(922) 6743 345</span>
                            </div>
                            <div className='inline-flex space-x-2 items-center  text-xl'>
                                <ion-icon
                                    name="mail"
                                ></ion-icon>
                                <span>cloudshipping@aup.edu.ph</span>
                            </div>
                            <div className='inline-flex space-x-2 items-center  text-xl'>
                                <ion-icon
                                    name="pin"
                                ></ion-icon>
                                <span>Puting Kahoy, Silang, Cavite 4118 Philippines</span>
                            </div>
                        </div>
                        <div className='flex space-x-4 text-lg '>
                            <a className="" href="https://www.facebook.com/profile.php?id=100073132796934">
                                <ion-icon name="logo-facebook"></ion-icon>
                            </a>
                            <a className="" href="https://twitter.com/cloud_shipping">
                                <ion-icon name="logo-twitter"></ion-icon>
                            </a>
                            <a className="" href="https://www.instagram.com/cloud.shipping/">
                                <ion-icon name="logo-instagram"></ion-icon>
                            </a>
                        </div>
                    </div>
                    <div className='relative'>
                        <div
                            className='absolute z-0 w-40 h-40 bg-tiffany rounded-full -right-28 -top-28'>
                        </div>
                        <div
                            className='absolute z-0 w-40 h-40 bg-tiffany rounded-full -left-28 -bottom-16'>
                        </div>
                        <div className='relative z-10 bg-white rounded-xl shadow-lg p-8 text-gray-600 md:w-80'>
                            <form action="" className='flex flex-col space-y-4'>
                                <div>
                                    <label htmlFor="" className='text-sm'>Your name</label>
                                    <input
                                        type="text"
                                        placeholder='Your name'
                                        className='ring-1 ring-1gray-300 w-full rounded-md px-4 py-2 mt-2 outline-none focus:ring-2 focus:ring-teal-500' />
                                </div>
                                <div>
                                    <label htmlFor="" className='text-sm'>Email Address</label>
                                    <input
                                        type="email"
                                        placeholder='Email Address'
                                        className='ring-1 ring-1gray-300 w-full rounded-md px-4 py-2 mt-2 outline-none focus:ring-2 focus:ring-teal-500' />
                                </div>
                                <div>
                                    <label htmlFor="" className='text-sm'>Message</label>
                                    <textarea
                                        placeholder='Message'
                                        rows='4'
                                        className='ring-1 ring-1gray-300 w-full rounded-md px-4 py-2
                                 mt-2 outline-none focus:ring-2 focus:ring-teal-500'
                                    ></textarea>
                                </div>
                                <button className='inline-block self-end bg-cyan-700 text-white 
                            font-bold rounded-lg px-6 py-2 uppercase text-sm'>
                                    Send message
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <Footer />
        </div>
    )
}

export default Contact
