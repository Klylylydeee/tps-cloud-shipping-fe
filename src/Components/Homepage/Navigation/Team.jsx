// NPM Packags
import React from "react";

// Components
import Navigation from "src/Components/Homepage/General/Navigation";
import Footer from "src/Components/Homepage/General/Footer";

// Assets
import Teamvalen from "src/Assets/valen.jpg";
import Teamklyde from "src/Assets/klyde.jpg";
import Teamkenneth from "src/Assets/kenneth.jpg";

// Obselete
// import Teamlee from "src/Assets/lee.jpg";
// import Teamrenato from "src/Assets/renato.jpg";
// import Teamzener from "src/Assets/zener.jpg";

const Team = () => {
    return (
        <div className="antialiased bg-tiffany-light">
            <Navigation />
            <div className="flex w-full min-h-screen justify-center items-center pt-20 pb-20">
                <div className="flex flex-col md:flex-row md:space-x-6 space-y-6 md:space-y-0 bg-dark-gray w-full max-w-4xl p-8 sm:p-12 rounded-xl shadow-lg text-white  overflow-hidden">
                    <div className="flex flex-col space-y-8 justify-between text-cyan-100">
                        <div className="container  mx-auto ">
                            <div className="flex flex-col text-center w-full pb-12">
                                <p className="text-4xl title-font mb-4 text-tiffany tracking-wide font-bold">
                                    About The Team
                                </p>
                                <p className="mx-auto leading-relaxed text-tiffany-light text-base">
                                    Teamwork is the ability to work together
                                    toward a common vision. The ability to
                                    direct individual accomplishments toward
                                    organizational objectives. It is the fuel
                                    that allows common people to attain uncommon
                                    results. ~ Andrew Carnegie
                                </p>
                            </div>
                            <div className="flex flex-wrap -m-4">
                                <div className="p-4 lg:w-1/2">
                                    <div className="h-full flex sm:flex-row flex-col items-center sm:justify-start justify-center text-center sm:text-left">
                                        <img
                                            src={Teamklyde}
                                            alt=""
                                            className="flex-shrink-0 rounded-lg w-48 h-48 object-cover object-center sm:mb-0 mb-4"
                                        />
                                        <div className="flex-grow sm:pl-8">
                                            <p className="title-font font-medium text-lg text-tiffany">
                                                Klyde Guevarra
                                            </p>
                                            <p className="text-tiffany mb-3">
                                                Leader (BE)
                                            </p>
                                            <p className="mb-4 text-tiffany-light text-sm">
                                                Klyde is a 4th year Information
                                                Technology Student of the
                                                Adventist University of the
                                                Philippines.
                                            </p>
                                            <span className="inline-flex">
                                                <a
                                                    href="https://www.facebook.com/guevarra.klyde"
                                                    className="text-tiffany-light"
                                                >
                                                    <svg
                                                        fill="none"
                                                        stroke="currentColor"
                                                        strokeLinecap="round"
                                                        strokeLinejoin="round"
                                                        strokeWidth="2"
                                                        className="w-5 h-5"
                                                        viewBox="0 0 24 24"
                                                    >
                                                        <path d="M18 2h-3a5 5 0 00-5 5v3H7v4h3v8h4v-8h3l1-4h-4V7a1 1 0 011-1h3z"></path>
                                                    </svg>
                                                </a>
                                                <a
                                                    href="https://github.com/Klylylydeee"
                                                    className="ml-2 text-tiffany-light"
                                                >
                                                    <svg
                                                        fill="none"
                                                        stroke="currentColor"
                                                        strokeLinecap="round"
                                                        strokeLinejoin="round"
                                                        strokeWidth="2"
                                                        className="w-5 h-5"
                                                        viewBox="0 0 24 24"
                                                    >
                                                        <path d="M21 11.5a8.38 8.38 0 01-.9 3.8 8.5 8.5 0 01-7.6 4.7 8.38 8.38 0 01-3.8-.9L3 21l1.9-5.7a8.38 8.38 0 01-.9-3.8 8.5 8.5 0 014.7-7.6 8.38 8.38 0 013.8-.9h.5a8.48 8.48 0 018 8v.5z"></path>
                                                    </svg>
                                                </a>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div className="p-4 lg:w-1/2">
                                    <div className="h-full flex sm:flex-row flex-col items-center sm:justify-start justify-center text-center sm:text-left">
                                        <img
                                            src={Teamvalen}
                                            alt=""
                                            className="flex-shrink-0 rounded-lg w-48 h-48 object-cover object-center sm:mb-0 mb-4"
                                        />
                                        <div className="flex-grow sm:pl-8">
                                            <p className="title-font font-medium text-lg text-tiffany">
                                                Valen Magante
                                            </p>
                                            <p className="text-tiffany mb-3">
                                                Member (FE)
                                            </p>
                                            <p className="mb-4 text-tiffany-light text-sm">
                                                Valen is a 3rd year Information
                                                Technology Student of the
                                                Adventist University of the
                                                Philippines.
                                            </p>
                                            <span className="inline-flex">
                                                <a
                                                    href="https://www.facebook.com/profile.php?id=100008410173116"
                                                    className="text-tiffany-light"
                                                >
                                                    <svg
                                                        fill="none"
                                                        stroke="currentColor"
                                                        strokeLinecap="round"
                                                        strokeLinejoin="round"
                                                        strokeWidth="2"
                                                        className="w-5 h-5"
                                                        viewBox="0 0 24 24"
                                                    >
                                                        <path d="M18 2h-3a5 5 0 00-5 5v3H7v4h3v8h4v-8h3l1-4h-4V7a1 1 0 011-1h3z"></path>
                                                    </svg>
                                                </a>
                                                <a
                                                    href="https://github.com/valen142001"
                                                    className="ml-2 text-tiffany-light"
                                                >
                                                    <svg
                                                        fill="none"
                                                        stroke="currentColor"
                                                        strokeLinecap="round"
                                                        strokeLinejoin="round"
                                                        strokeWidth="2"
                                                        className="w-5 h-5"
                                                        viewBox="0 0 24 24"
                                                    >
                                                        <path d="M21 11.5a8.38 8.38 0 01-.9 3.8 8.5 8.5 0 01-7.6 4.7 8.38 8.38 0 01-3.8-.9L3 21l1.9-5.7a8.38 8.38 0 01-.9-3.8 8.5 8.5 0 014.7-7.6 8.38 8.38 0 013.8-.9h.5a8.48 8.48 0 018 8v.5z"></path>
                                                    </svg>
                                                </a>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div className="p-4 lg:w-1/2">
                                    <div className="h-full flex sm:flex-row flex-col items-center sm:justify-start justify-center text-center sm:text-left">
                                        <img
                                            src={Teamkenneth}
                                            alt=""
                                            className="flex-shrink-0 rounded-lg w-48 h-48 object-cover object-center sm:mb-0 mb-4"
                                        />
                                        <div className="flex-grow sm:pl-8">
                                            <p className="title-font font-medium text-lg text-tiffany">
                                                Kenneth Canedo
                                            </p>
                                            <p className="text-tiffany mb-3">
                                                Member (FE)
                                            </p>
                                            <p className="mb-4 text-tiffany-light text-sm">
                                                Kenneth is a 3rd year
                                                Information Technology Student
                                                of the Adventist University of
                                                the Philippines.
                                            </p>
                                            <span class="inline-flex">
                                                <a
                                                    href="https://www.facebook.com/keeejac"
                                                    className="text-tiffany-light"
                                                >
                                                    <svg
                                                        fill="none"
                                                        stroke="currentColor"
                                                        strokeLinecap="round"
                                                        strokeLinejoin="round"
                                                        strokeWidth="2"
                                                        class="w-5 h-5"
                                                        viewBox="0 0 24 24"
                                                    >
                                                        <path d="M18 2h-3a5 5 0 00-5 5v3H7v4h3v8h4v-8h3l1-4h-4V7a1 1 0 011-1h3z"></path>
                                                    </svg>
                                                </a>
                                                <a
                                                    href="https://github.com/Klylylydeee/tps-cloud-shipping-fe"
                                                    className="ml-2 text-tiffany-light"
                                                >
                                                    <svg
                                                        fill="none"
                                                        stroke="currentColor"
                                                        strokeLinecap="round"
                                                        strokeLinejoin="round"
                                                        strokeWidth="2"
                                                        className="w-5 h-5"
                                                        viewBox="0 0 24 24"
                                                    >
                                                        <path d="M21 11.5a8.38 8.38 0 01-.9 3.8 8.5 8.5 0 01-7.6 4.7 8.38 8.38 0 01-3.8-.9L3 21l1.9-5.7a8.38 8.38 0 01-.9-3.8 8.5 8.5 0 014.7-7.6 8.38 8.38 0 013.8-.9h.5a8.48 8.48 0 018 8v.5z"></path>
                                                    </svg>
                                                </a>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="relative">
                        <div className="absolute z-0 w-40 h-40 bg-tiffany  rounded-full -right-28 -top-28"></div>
                        <div className="absolute z-0 w-40 h-40 bg-tiffany  rounded-full -left-80 -bottom-28"></div>
                    </div>
                </div>
            </div>
            <Footer />
        </div>
    );
};

export default Team;
