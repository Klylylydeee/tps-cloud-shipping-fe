// NPM Packages
import React from "react";
import { Link } from "react-router-dom";

// Components
import Navigation from "src/Components/Homepage/General/Navigation";
import Footer from "src/Components/Homepage/General/Footer";

// Assets
import cloudLogo from "src/Assets/logoPartner.png";
import dapsLogo from "src/Assets/Daps.png";
import eisLogo from "src/Assets/EIS.png";
import imsLogo from "src/Assets/IMS.png";
import sisLogo from "src/Assets/SIS.png";




const Partner = () => {
    return (
        <div className="antialiased bg-tiffany-light">
            <Navigation />
            <div className="flex w-full min-h-screen justify-center items-center pt-20 pb-20">
                <div className="flex flex-col md:flex-row md:space-x-6 space-y-6 md:space-y-0 bg-dark-gray w-full max-w-4xl p-8 sm:p-12 rounded-xl shadow-lg text-white  overflow-hidden">
                    <div className="flex flex-col space-y-8 justify-between text-cyan-100">
                        <div className="container  mx-auto ">
                            <div className="flex flex-col text-center w-full pb-5">
                                <p className="text-4xl title-font text-tiffany tracking-wide font-bold">
                                    Partners
                                </p>
                            </div>
                            <div className="flex flex-wrap">
                                <div className="p-4 lg:w-1/3">
                                    <div className="h-full flex flex-col items-center sm:justify-start justify-center text-center sm:text-left">
                                        <div className="flex-col md-4 ">
                                            <img
                                                src={imsLogo}
                                                alt=""
                                                className="rounded-lg"
                                            />
                                            <p className="pt-2 text-sm">
                                                Lorem Ipsum like Aldus PageMaker
                                                including versions of Lorem Ipsum.
                                            </p>
                                            <Link to="/">
                                                <button className="font-bold hover:text-indigo-300 pt-3 pb-10 ">
                                                    Read More
                                                </button>
                                            </Link>
                                        </div>
                                    </div>
                                </div>
                                <div className="p-4 lg:w-1/3">
                                    <div className="h-full flex flex-col items-center sm:justify-start justify-center text-center sm:text-left">
                                        <div className="flex-col md-4 ">
                                            <img
                                                src={dapsLogo}
                                                alt=""
                                                className="rounded-lg"
                                            />
                                            <p className="pt-2 text-sm">
                                                Lorem Ipsum like Aldus PageMaker
                                                including versions of Lorem Ipsum.
                                            </p>
                                            <Link to="/">
                                                <button className="font-bold hover:text-indigo-300 pt-3 pb-10 ">
                                                    Read More
                                                </button>
                                            </Link>
                                        </div>
                                    </div>
                                </div>
                                <div className="p-4 lg:w-1/3">
                                    <div className="h-full flex flex-col items-center sm:justify-start justify-center text-center sm:text-left">
                                        <div className="flex-col md-4 ">
                                            <img
                                                src={eisLogo}
                                                alt=""
                                                className="rounded-lg"
                                            />
                                            <p className="pt-2 text-sm">
                                                Lorem Ipsum like Aldus PageMaker
                                                including versions of Lorem Ipsum.
                                            </p>
                                            <Link to="/">
                                                <button className="font-bold hover:text-indigo-300 pt-3 pb-10 ">
                                                    Read More
                                                </button>
                                            </Link>
                                        </div>
                                    </div>
                                </div>
                                <div className="p-4 lg:w-1/3">
                                    <div className="h-full flex flex-col items-center sm:justify-start justify-center text-center sm:text-left">
                                        <div className="flex-col md-4 ">
                                            <img
                                                src={sisLogo}
                                                alt=""
                                                className="rounded-lg"
                                            />
                                            <p className="pt-2 text-sm">
                                                Lorem Ipsum like Aldus PageMaker
                                                including versions of Lorem Ipsum.
                                            </p>
                                            <Link to="/">
                                                <button className="font-bold hover:text-indigo-300 pt-3 pb-10 ">
                                                    Read More
                                                </button>
                                            </Link>
                                        </div>
                                    </div>
                                </div>
                                <div className="p-4 lg:w-1/3">
                                    <div className="h-full flex flex-col items-center sm:justify-start justify-center text-center sm:text-left">
                                        <div className="flex-col md-4 ">
                                            <img
                                                src={cloudLogo}
                                                alt=""
                                                className="rounded-lg"
                                            />
                                            <p className="pt-2 text-sm">
                                                Cloud Sipping provides services that
                                                fulfills individauls, business owners,
                                                and large organizations shipping
                                                transaction in a delightful experience
                                                through managing transactions and
                                                automate all your shipping needs.
                                            </p>
                                            <Link to="/">
                                                <button className="font-bold hover:text-indigo-300 pt-3 pb-10 ">
                                                    Read More
                                                </button>
                                            </Link>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="relative">
                        <div className="absolute z-0 w-40 h-40 bg-tiffany  rounded-full -right-28 -top-28"></div>
                        <div className="absolute z-0 w-40 h-40 bg-tiffany  rounded-full -left-80 -bottom-28"></div>
                    </div>
                </div>
            </div>
            <Footer />
        </div>
    );
};

export default Partner;
