// NPM Packages
import React from "react";
import { FaShippingFast, FaBusinessTime, FaMapMarkerAlt } from "react-icons/fa";
import { Ri24HoursLine, RiVipLine } from "react-icons/ri";
import { GiCctvCamera } from "react-icons/gi";

// Components
import Navigation from "src/Components/Homepage/General/Navigation";
import Footer from "src/Components/Homepage/General/Footer";

const Services = () => {
    return (
        <div className="antialiased bg-tiffany-light">
            <Navigation />
            <div className="flex w-full min-h-screen justify-center items-center pt-20 pb-20">
                <div className="flex flex-col md:flex-row md:space-x-6 space-y-6 md:space-y-0 bg-dark-gray w-full max-w-4xl p-8 sm:p-12 rounded-xl shadow-lg text-white  overflow-hidden">
                    <div className="flex flex-col space-y-8 justify-between text-cyan-100">
                        <div className="container  mx-auto">
                            <div className="flex flex-col text-center w-full pb-5">
                                <p className="text-4xl title-font text-tiffany tracking-wide font-bold">
                                    Services
                                </p>
                            </div>
                            <div className="flex flex-wrap">
                                <div className="p-4 lg:w-1/3">
                                    <div className="h-full flex flex-col items-center sm:justify-start justify-center text-center sm:text-left">
                                        <div className="">
                                            <FaShippingFast size="7em" />
                                        </div>
                                        <p className="font-bold text-tiffany">
                                            Fast and on-time delivery
                                        </p>
                                        <p className="text-sm">
                                            Our efficient distribution system
                                            ensures that packages always arrive
                                            on-time.
                                        </p>
                                    </div>
                                </div>
                                <div className="p-4 lg:w-1/3">
                                    <div className="h-full flex flex-col items-center sm:justify-start justify-center text-center sm:text-left">
                                        <div className="">
                                            <Ri24HoursLine size="7em" />
                                        </div>
                                        <p className="font-bold text-tiffany">
                                            24/7 customer service
                                        </p>
                                        <p className="text-sm">
                                            Customer service is available
                                            anytime at +(922) 6743 345.
                                        </p>
                                    </div>
                                </div>
                                <div className="p-4 lg:w-1/3">
                                    <div className="h-full flex flex-col items-center sm:justify-start justify-center text-center sm:text-left">
                                        <div className="">
                                            <FaBusinessTime size="7em" />
                                        </div>
                                        <p className="font-bold text-tiffany">
                                            Fast Claim Systems
                                        </p>
                                        <p className="text-sm">
                                            Guarantee of compensation for damage
                                            and loss of package.
                                        </p>
                                    </div>
                                </div>
                                <div className="p-4 lg:w-1/3">
                                    <div className="h-full flex flex-col items-center sm:justify-start justify-center text-center sm:text-left">
                                        <div className="">
                                            <FaMapMarkerAlt size="7em" />
                                        </div>
                                        <p className="font-bold text-tiffany">
                                            GPS Tracking System
                                        </p>
                                        <p className="text-sm">
                                            Delivery trucks are monitored by GPS
                                            tracking to ensure on-time
                                            deliveries.
                                        </p>
                                    </div>
                                </div>
                                <div className="p-4 lg:w-1/3">
                                    <div className="h-full flex flex-col items-center sm:justify-start justify-center text-center sm:text-left">
                                        <div className="">
                                            <GiCctvCamera size="7em" />
                                        </div>
                                        <p className="font-bold text-tiffany">
                                            24/7 CCT Monitoring
                                        </p>
                                        <p className="text-sm">
                                            Every stop of packages are monitored
                                            by an advanced CCTV system to ensure
                                            security and safety of packages..
                                        </p>
                                    </div>
                                </div>
                                <div className="p-4 lg:w-1/3">
                                    <div className="h-full flex flex-col items-center sm:justify-start justify-center text-center sm:text-left">
                                        <div className="">
                                            <RiVipLine size="7em" />
                                        </div>
                                        <p className="font-bold text-tiffany">
                                            VIP Platform
                                        </p>
                                        <p className="relative z-10 text-sm">
                                            Self-service that provides the
                                            online sellers with tools to
                                            accelerate the process of delivery
                                            of packages and activities.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    <div className="relative">
                        <div className="absolute z-0 w-40 h-40 bg-tiffany  rounded-full -right-28 -top-28"></div>
                        <div className="absolute z-0 w-40 h-40 bg-tiffany  rounded-full -left-80 -bottom-28"></div>
                    </div>
                </div>
            </div>
            <Footer />
        </div>
    );
};

export default Services;
