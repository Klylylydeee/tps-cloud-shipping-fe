// NPM Packages
import React from "react";
import { Link } from "react-router-dom";

// Assets
import ManilaCity from "src/Assets/manila.jpg";
import Outside from "src/Assets/outside.jpg";

const Services = () => {
    return (
        <section className="bg-dark-gray">
            <div className="container px-5 py-28 mx-auto">
                <div className="flex flex-col text-center w-full mb-20">
                    <p className="text-5xl font-medium title-font mb-4 text-tiffany-light tracking-widest">
                        SERVICES
                    </p>
                    <p className="lg:w-2/3 mx-auto leading-relaxed text-base text-tiffany">
                        Cloud shipping provides affordable, reliable and same
                        day delivery service in your area.
                    </p>
                </div>
                <div className="flex flex-wrap -mx-4 -mb-10 text-center">
                    <div className="sm:w-1/2 mb-10 px-4">
                        <div className=" h-64 overflow-hidden">
                            <img
                                src={ManilaCity}
                                alt=""
                                className="object-contain h-100 W w-full"
                            />
                        </div>
                        <h2 className="title-font text-2xl font-medium text-tiffany-light mt-6 mb-3">
                            Delivery within Manila
                        </h2>
                        <p className="leading-relaxed text-base mb-8 text-tiffany">
                            The fastest door-to-door delivery service will come
                            to your area.{" "}
                        </p>
                        <Link
                            to="/services"
                            class="text-tiffany-light bg-transparent border border-tiffany py-2 px-8 focus:outline-none hover:bg-tiffany rounded text-lg"
                        >
                            Learn More
                        </Link>
                    </div>
                    <div className="sm:w-1/2 mb-10 px-4">
                        <div className=" h-64 overflow-hidden">
                            <img
                                src={Outside}
                                alt=""
                                className="object-contain h-100 W w-full"
                            />
                        </div>
                        <h2 className="title-font text-2xl font-medium  mt-6 mb-3 text-tiffany-light">
                            Delivery outside Manila
                        </h2>
                        <p className="leading-relaxed text-base mb-8 text-tiffany">
                            On time delivery, we won't keep you waiting for
                            long.
                        </p>
                        <Link
                            to="/services"
                            className="text-tiffany-light bg-transparent border border-tiffany py-2 px-8 focus:outline-none hover:bg-tiffany rounded text-lg"
                        >
                            Learn More
                        </Link>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default Services;
