// NPM Packages
import React from "react";
import { Link } from "react-router-dom";

// Assets
import City from "src/Assets/cityroad.jpeg";

const Hero = () => {
    return (
        <div className="md:flex items-center justify-center relative h-screen">
            <img
                src={City}
                alt=""
                className="absolute h-full w-full object-cover"
            />
            <div className="px-5 py-24 mb-24 text-center justify-center relative">
                <p className="text-4xl text-tiffany font-bold tracking-normal ">
                    {" "}
                    Transforming the Shipping and Business Industry for the Next
                    Generation
                </p>
                <p className="mb-10 text-1xl pt-6 text-tiffany tracking-normal ">
                    Cloud Shipping provides services that fulfills individuals,
                    business owners, and large organizations shipping
                    transaction in a delightful experience through managing
                    transactions and automate all your shipping needs
                </p>
                <Link
                    to="/sign-in"
                    className=" mr-8 mx-auto text-white bg-tiffany border border-tiffany hover:border-transparent rounded py-2 px-8 focus:outline-none hover:bg-indigo-600 rounded text-lg"
                >
                    Sign In
                </Link>
                <Link
                    to="/sign-up"
                    className="mx-auto text-white bg-transparent border border-tiffany py-2 px-8 focus:outline-none hover:bg-indigo-600 rounded text-lg"
                >
                    Sign Up
                </Link>
            </div>
        </div>
    );
};

export default Hero;
