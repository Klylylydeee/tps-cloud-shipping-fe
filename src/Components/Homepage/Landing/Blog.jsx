// NPM Packages
import React from "react";

// Assets
import Blog1 from "src/Assets/blog1.jpg";
import Blog2 from "src/Assets/blog2.jpg";
import Blog4 from "src/Assets/blog4.jpg";
import Blog3 from "src/Assets/blog3.jpg";
import Blog5 from "src/Assets/blog5.jpg";
import Blog6 from "src/Assets/blog6.jpg";

const Blog = () => {
    return (
        <section className="bg-tiffany-light ">
            <div className="container px-5 py-24 mx-auto ">
                <div className="flex flex-col text-center w-full mb-20">
                    <h1 className="text-5xl font-medium title-font mb-4 text-gray-900 tracking-widest">
                        BLOG
                    </h1>
                    <p className="lg:w-2/3 mx-auto leading-relaxed text-base">
                        Check cloud shipping latest post from our blog.{" "}
                    </p>
                </div>
                <div className="flex flex-wrap -m-4">
                    <div className="p-4 md:w-1/3">
                        <div className="h-full  rounded-lg overflow-hidden">
                            <img
                                src={Blog1}
                                alt=""
                                className="object-contain h-30 w-30 "
                            />
                            <div className="p-6">
                                <p className=" text-lg title-font font-medium font-bold text-dark-gray mb-3">
                                    Cloud Shipping Remains Open Amid COVID-19
                                    Pandemic
                                </p>
                                <p className="tracking-widest title-font text-xs  font-medium text-gray-900 mb-1">
                                     August 7, 2021
                                </p>
                                <p className="leading-relaxed mb-3">
                                    We consider it a privilege to continue
                                    serving our clients even in the midst of a
                                    global health crisis. We put the safety of
                                    our drivers and our clients first.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div className="p-4 md:w-1/3">
                        <div className="h-full  rounded-lg overflow-hidden">
                            <img
                                src={Blog2}
                                alt=""
                                className="object-contain h-30 w-30 "
                            />
                            <div className="p-6">
                                <p className="text-lg title-font font-medium font-bold text-dark-gray mb-3">
                                    The Importance of a Reliable Delivery
                                    Service
                                </p>
                                <p className="tracking-widest  title-font text-xs font-medium text-gray-900 mb-1">
                                    August 12, 2021
                                </p>
                                <p className="leading-relaxed mb-3">
                                    Your package has traveled a great distance.
                                    But due to that great distance, it may have
                                    had to switch hands once or a few times. But
                                    making sure that the last leg of the trip
                                    goes smoothly can be the most important,
                                    ensuring that your packages reach their
                                    destination quickly for a timely arrival.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div className="p-4 md:w-1/3">
                        <div className="h-full  rounded-lg overflow-hidden">
                            <img
                                src={Blog3}
                                alt=""
                                className="object-contain h-30 w-30 "
                            />
                            <div className="p-6">
                                <p className=" text-lg title-font font-medium font-bold text-dark-gray mb-3">
                                    Getting Your Deliveries Safely
                                </p>
                                <p className="tracking-widest title-font text-xs font-medium text-gray-900 mb-1">
                                    September 2, 2021
                                </p>
                                <p className="leading-relaxed mb-3">
                                    In these uncertain times, there is greater
                                    uncertainty about how to bring your packages
                                    covid-free into your home. What we do is we
                                    sanitize and clean your packages, so that
                                    your home will be well-supplied, while you
                                    stay well and safe
                                </p>
                            </div>
                        </div>
                    </div>
                    <div className="p-4 md:w-1/3">
                        <div className="h-full rounded-lg overflow-hidden">
                            <img
                                src={Blog4}
                                alt=""
                                className="object-contain h-30 w-30 "
                            />
                            <div className="p-6">
                                <p className="text-lg title-font font-medium font-bold text-dark-gray mb-3">
                                    Top 3 Reasons To Choose Cloud Shipping
                                </p>
                                <p className="tracking-widest  title-font text-xs font-medium text-gray-900 mb-1">
                                    September 12, 2021
                                </p>
                                <p className="leading-relaxed mb-3">
                                    <span className="font-bold ">
                                        Affordability;{" "}
                                    </span>
                                    cloud Shipping offer more affordable rates
                                    compared to larger companies with
                                    standardized prices.
                                    <span className="font-bold ">
                                        {" "}
                                        Same-Day Delivery Options;{" "}
                                    </span>{" "}
                                    cloud shipping provide the fastest delivery
                                    service in your area.{" "}
                                    <span className="font-bold ">
                                        Reliability;{" "}
                                    </span>
                                    cloud shipping offer tracking features to
                                    give you a stronger sense of security
                                    knowing where your package is every step of
                                    the way.{" "}
                                </p>
                            </div>
                        </div>
                    </div>
                    <div className="p-4 md:w-1/3">
                        <div className="h-full  rounded-lg overflow-hidden">
                            <img
                                src={Blog5}
                                alt=""
                                className="object-contain h-30 w-30 "
                            />
                            <div className="p-6">
                                <p className=" text-lg title-font font-medium font-bold text-dark-gray mb-3">
                                    Same-Day Delivery Services Can Save the Day
                                </p>
                                <p className="tracking-widest title-font text-xs font-medium text-gray-900 mb-1">
                                    October 1, 2021
                                </p>
                                <p className="leading-relaxed mb-3">
                                    There are some cases where you might find
                                    yourself short on time, but you absolutely
                                    need to get a delivery made. Maybe you have
                                    to go to work, or you have an important
                                    event that can’t be delayed, but somehow you
                                    need the shipment made without being in two
                                    places at once. That’s when a same-day rush
                                    courier can come in handy and save the day
                                </p>
                            </div>
                        </div>
                    </div>
                    <div className="p-4 md:w-1/3">
                        <div className="h-full rounded-lg overflow-hidden">
                            <img
                                src={Blog6}
                                alt=""
                                className="object-contain h-30 w-30 "
                            />
                            <div className="p-6">
                                <p className="text-lg title-font font-medium font-bold text-dark-gray mb-3">
                                    The Top Delivery Services in the Philippines
                                </p>
                                <p className="tracking-widest title-font text-xs font-medium text-gray-900 mb-1">
                                    October 17, 2021
                                </p>
                                <p className="leading-relaxed mb-3">
                                    Cloud Shipping, Toktok, Transportify, Mr.
                                    Speedy, Grab, Ninja Van. Among the delivery
                                    services offer are parcel delivery,
                                    documents delivery, and delivery of the big
                                    item, etc.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default Blog;
