// NPM Packages
import React from "react";

// Assets
import Teamvalen from "src/Assets/valen.jpg";
import ViceG from "src/Assets/vice.jpg";
import JhongH from "src/Assets/jhong.jpg";
import KarylleG from "src/Assets/Karylle.jpg";
import Teamzener from "src/Assets/zener.jpg";
import Teamklyde from "src/Assets/klyde.jpg";

const Review = () => {
    return (
        <section className="bg-dark-gray">
            <div className="container px-5 py-24 mx-auto ">
                <div className="flex flex-col text-center w-full mb-20">
                    <h1 className="text-5xl font-medium title-font mb-4 text-tiffany-light tracking-widest">
                        REVIEW ABOUT US
                    </h1>
                    <p className="lg:w-2/3 mx-auto leading-relaxed text-tiffany text-base">
                        We ask our customers to submit a review of their Cloud
                        Shipping experience 1 week after their order was placed.
                        Here are those reviews, good and bad, giving you an
                        honest insight into what you can expect from Cloud
                        Shipping.
                    </p>
                </div>
                <div className="flex flex-wrap -mx-4 -my-8">
                    <div className="py-8 px-4 lg:w-1/3">
                        <div className="h-full flex items-start">
                            <div className="flex-grow pl-6">
                                <h2 className="tracking-widest text-xs title-font font-medium text-tiffany-light mb-1">
                                    August 20, 2021
                                </h2>
                                <div className="flex justify-left items-left">
                                    <div className="flex items-left mt-2 mb-4">
                                        <svg
                                            className="mx-1 w-4 h-4 fill-current text-yellow-500"
                                            xmlns="http://www.w3.org/2000/svg"
                                            viewBox="0 0 20 20"
                                        >
                                            <path d="M10 15l-5.878 3.09 1.123-6.545L.489 6.91l6.572-.955L10 0l2.939 5.955 6.572.955-4.756 4.635 1.123 6.545z" />
                                        </svg>
                                        <svg
                                            className="mx-1 w-4 h-4 fill-current text-yellow-500"
                                            xmlns="http://www.w3.org/2000/svg"
                                            viewBox="0 0 20 20"
                                        >
                                            <path d="M10 15l-5.878 3.09 1.123-6.545L.489 6.91l6.572-.955L10 0l2.939 5.955 6.572.955-4.756 4.635 1.123 6.545z" />
                                        </svg>
                                        <svg
                                            className="mx-1 w-4 h-4 fill-current text-yellow-500"
                                            xmlns="http://www.w3.org/2000/svg"
                                            viewBox="0 0 20 20"
                                        >
                                            <path d="M10 15l-5.878 3.09 1.123-6.545L.489 6.91l6.572-.955L10 0l2.939 5.955 6.572.955-4.756 4.635 1.123 6.545z" />
                                        </svg>
                                        <svg
                                            className="mx-1 w-4 h-4 fill-current text-yellow-500"
                                            xmlns="http://www.w3.org/2000/svg"
                                            viewBox="0 0 20 20"
                                        >
                                            <path d="M10 15l-5.878 3.09 1.123-6.545L.489 6.91l6.572-.955L10 0l2.939 5.955 6.572.955-4.756 4.635 1.123 6.545z" />
                                        </svg>
                                        <svg
                                            className="mx-1 w-4 h-4 fill-current text-yellow-500"
                                            xmlns="http://www.w3.org/2000/svg"
                                            viewBox="0 0 20 20"
                                        >
                                            <path d="M10 15l-5.878 3.09 1.123-6.545L.489 6.91l6.572-.955L10 0l2.939 5.955 6.572.955-4.756 4.635 1.123 6.545z" />
                                        </svg>
                                    </div>
                                </div>
                                <p className="leading-relaxed mb-5 text-tiffany">
                                    Fantastic service from start to finish. So
                                    reasonable too. Parcel arrived at
                                    destination safe and sound.
                                </p>
                                <div className="inline-flex items-center">
                                    <img
                                        src={ViceG}
                                        alt=""
                                        className="w-12 h-12 rounded-full flex-shrink-0 object-cover object-center"
                                    />
                                    <span className="flex-grow flex flex-col pl-3">
                                        <span className="title-font font-medium text-tiffany">
                                            Jamie Cruz
                                        </span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="py-8 px-4 lg:w-1/3">
                        <div className="h-full flex items-start">
                            <div className="flex-grow pl-6">
                                <h2 className="tracking-widest text-xs title-font font-medium text-tiffany-light mb-1">
                                    August 27, 2021
                                </h2>
                                <div className="flex justify-left items-left">
                                    <div className="flex items-left mt-2 mb-4">
                                        <svg
                                            className="mx-1 w-4 h-4 fill-current text-yellow-500"
                                            xmlns="http://www.w3.org/2000/svg"
                                            viewBox="0 0 20 20"
                                        >
                                            <path d="M10 15l-5.878 3.09 1.123-6.545L.489 6.91l6.572-.955L10 0l2.939 5.955 6.572.955-4.756 4.635 1.123 6.545z" />
                                        </svg>
                                        <svg
                                            className="mx-1 w-4 h-4 fill-current text-yellow-500"
                                            xmlns="http://www.w3.org/2000/svg"
                                            viewBox="0 0 20 20"
                                        >
                                            <path d="M10 15l-5.878 3.09 1.123-6.545L.489 6.91l6.572-.955L10 0l2.939 5.955 6.572.955-4.756 4.635 1.123 6.545z" />
                                        </svg>
                                        <svg
                                            className="mx-1 w-4 h-4 fill-current text-yellow-500"
                                            xmlns="http://www.w3.org/2000/svg"
                                            viewBox="0 0 20 20"
                                        >
                                            <path d="M10 15l-5.878 3.09 1.123-6.545L.489 6.91l6.572-.955L10 0l2.939 5.955 6.572.955-4.756 4.635 1.123 6.545z" />
                                        </svg>
                                        <svg
                                            className="mx-1 w-4 h-4 fill-current text-yellow-500"
                                            xmlns="http://www.w3.org/2000/svg"
                                            viewBox="0 0 20 20"
                                        >
                                            <path d="M10 15l-5.878 3.09 1.123-6.545L.489 6.91l6.572-.955L10 0l2.939 5.955 6.572.955-4.756 4.635 1.123 6.545z" />
                                        </svg>
                                        <svg
                                            className="mx-1 w-4 h-4 fill-current text-gray-400"
                                            xmlns="http://www.w3.org/2000/svg"
                                            viewBox="0 0 20 20"
                                        >
                                            <path d="M10 15l-5.878 3.09 1.123-6.545L.489 6.91l6.572-.955L10 0l2.939 5.955 6.572.955-4.756 4.635 1.123 6.545z" />
                                        </svg>
                                    </div>
                                </div>
                                <p className="leading-relaxed mb-5 text-tiffany">
                                    The parcel was delivered in a totally
                                    professional manner and I will be using
                                    Cloud shipping again.
                                </p>
                                <div className="inline-flex items-center">
                                    <img
                                        src={KarylleG}
                                        alt=""
                                        className="w-12 h-12 rounded-full flex-shrink-0 object-cover object-center"
                                    />
                                    <span className="flex-grow flex flex-col pl-3">
                                        <span className="title-font font-medium text-tiffany">
                                            Kate Santos
                                        </span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="py-8 px-4 lg:w-1/3">
                        <div className="h-full flex items-start">
                            <div className="flex-grow pl-6">
                                <h2 className="tracking-widest text-xs title-font font-medium text-tiffany-light mb-1">
                                    September 14, 2021
                                </h2>
                                <div className="flex justify-left items-left">
                                    <div className="flex items-left mt-2 mb-4">
                                        <svg
                                            className="mx-1 w-4 h-4 fill-current text-yellow-500"
                                            xmlns="http://www.w3.org/2000/svg"
                                            viewBox="0 0 20 20"
                                        >
                                            <path d="M10 15l-5.878 3.09 1.123-6.545L.489 6.91l6.572-.955L10 0l2.939 5.955 6.572.955-4.756 4.635 1.123 6.545z" />
                                        </svg>
                                        <svg
                                            className="mx-1 w-4 h-4 fill-current text-yellow-500"
                                            xmlns="http://www.w3.org/2000/svg"
                                            viewBox="0 0 20 20"
                                        >
                                            <path d="M10 15l-5.878 3.09 1.123-6.545L.489 6.91l6.572-.955L10 0l2.939 5.955 6.572.955-4.756 4.635 1.123 6.545z" />
                                        </svg>
                                        <svg
                                            className="mx-1 w-4 h-4 fill-current text-yellow-500"
                                            xmlns="http://www.w3.org/2000/svg"
                                            viewBox="0 0 20 20"
                                        >
                                            <path d="M10 15l-5.878 3.09 1.123-6.545L.489 6.91l6.572-.955L10 0l2.939 5.955 6.572.955-4.756 4.635 1.123 6.545z" />
                                        </svg>
                                        <svg
                                            className="mx-1 w-4 h-4 fill-current text-yellow-500"
                                            xmlns="http://www.w3.org/2000/svg"
                                            viewBox="0 0 20 20"
                                        >
                                            <path d="M10 15l-5.878 3.09 1.123-6.545L.489 6.91l6.572-.955L10 0l2.939 5.955 6.572.955-4.756 4.635 1.123 6.545z" />
                                        </svg>
                                        <svg
                                            className="mx-1 w-4 h-4 fill-current text-yellow-500"
                                            xmlns="http://www.w3.org/2000/svg"
                                            viewBox="0 0 20 20"
                                        >
                                            <path d="M10 15l-5.878 3.09 1.123-6.545L.489 6.91l6.572-.955L10 0l2.939 5.955 6.572.955-4.756 4.635 1.123 6.545z" />
                                        </svg>
                                    </div>
                                </div>
                                <p className="leading-relaxed mb-5 text-tiffany">
                                    Website easy to use. I will recommend it to
                                    others, very efficient. Job well done!
                                    Thankyou.
                                </p>
                                <div className="inline-flex items-center">
                                    <img
                                        src={JhongH}
                                        alt=""
                                        className="w-12 h-12 rounded-full flex-shrink-0 object-cover object-center"
                                    />
                                    <span className="flex-grow flex flex-col pl-3">
                                        <span className="title-font font-medium text-tiffany">
                                            Lany Smith
                                        </span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="py-8 px-4 lg:w-1/3">
                        <div className="h-full flex items-start">
                            <div className="flex-grow pl-6">
                                <h2 className="tracking-widest text-xs title-font font-medium text-tiffany-light mb-1">
                                    September 15, 2021
                                </h2>
                                <div className="flex justify-left items-left">
                                    <div className="flex items-left mt-2 mb-4">
                                        <svg
                                            className="mx-1 w-4 h-4 fill-current text-yellow-500"
                                            xmlns="http://www.w3.org/2000/svg"
                                            viewBox="0 0 20 20"
                                        >
                                            <path d="M10 15l-5.878 3.09 1.123-6.545L.489 6.91l6.572-.955L10 0l2.939 5.955 6.572.955-4.756 4.635 1.123 6.545z" />
                                        </svg>
                                        <svg
                                            className="mx-1 w-4 h-4 fill-current text-yellow-500"
                                            xmlns="http://www.w3.org/2000/svg"
                                            viewBox="0 0 20 20"
                                        >
                                            <path d="M10 15l-5.878 3.09 1.123-6.545L.489 6.91l6.572-.955L10 0l2.939 5.955 6.572.955-4.756 4.635 1.123 6.545z" />
                                        </svg>
                                        <svg
                                            className="mx-1 w-4 h-4 fill-current text-yellow-500"
                                            xmlns="http://www.w3.org/2000/svg"
                                            viewBox="0 0 20 20"
                                        >
                                            <path d="M10 15l-5.878 3.09 1.123-6.545L.489 6.91l6.572-.955L10 0l2.939 5.955 6.572.955-4.756 4.635 1.123 6.545z" />
                                        </svg>
                                        <svg
                                            className="mx-1 w-4 h-4 fill-current text-yellow-500"
                                            xmlns="http://www.w3.org/2000/svg"
                                            viewBox="0 0 20 20"
                                        >
                                            <path d="M10 15l-5.878 3.09 1.123-6.545L.489 6.91l6.572-.955L10 0l2.939 5.955 6.572.955-4.756 4.635 1.123 6.545z" />
                                        </svg>
                                        <svg
                                            className="mx-1 w-4 h-4 fill-current text-gray-400"
                                            xmlns="http://www.w3.org/2000/svg"
                                            viewBox="0 0 20 20"
                                        >
                                            <path d="M10 15l-5.878 3.09 1.123-6.545L.489 6.91l6.572-.955L10 0l2.939 5.955 6.572.955-4.756 4.635 1.123 6.545z" />
                                        </svg>
                                    </div>
                                </div>
                                <p className="leading-relaxed mb-5 text-tiffany">
                                    Good service. First time using cloud
                                    shipping,very satisfied with the
                                    outcome,highly recommend. Thank you!
                                </p>
                                <div className="inline-flex items-center">
                                    <img
                                        src={Teamvalen}
                                        alt=""
                                        className="w-12 h-12 rounded-full flex-shrink-0 object-cover object-center"
                                    />
                                    <span className="flex-grow flex flex-col pl-3">
                                        <span className="title-font font-medium text-tiffany">
                                            Kim Swift
                                        </span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="py-8 px-4 lg:w-1/3">
                        <div className="h-full flex items-start">
                            <div className="flex-grow pl-6">
                                <h2 className="tracking-widest text-xs title-font font-medium text-tiffany-light mb-1">
                                    September 27, 2021
                                </h2>
                                <div class="flex justify-left items-left">
                                    <div className="flex items-left mt-2 mb-4">
                                        <svg
                                            className="mx-1 w-4 h-4 fill-current text-yellow-500"
                                            xmlns="http://www.w3.org/2000/svg"
                                            viewBox="0 0 20 20"
                                        >
                                            <path d="M10 15l-5.878 3.09 1.123-6.545L.489 6.91l6.572-.955L10 0l2.939 5.955 6.572.955-4.756 4.635 1.123 6.545z" />
                                        </svg>
                                        <svg
                                            className="mx-1 w-4 h-4 fill-current text-yellow-500"
                                            xmlns="http://www.w3.org/2000/svg"
                                            viewBox="0 0 20 20"
                                        >
                                            <path d="M10 15l-5.878 3.09 1.123-6.545L.489 6.91l6.572-.955L10 0l2.939 5.955 6.572.955-4.756 4.635 1.123 6.545z" />
                                        </svg>
                                        <svg
                                            className="mx-1 w-4 h-4 fill-current text-yellow-500"
                                            xmlns="http://www.w3.org/2000/svg"
                                            viewBox="0 0 20 20"
                                        >
                                            <path d="M10 15l-5.878 3.09 1.123-6.545L.489 6.91l6.572-.955L10 0l2.939 5.955 6.572.955-4.756 4.635 1.123 6.545z" />
                                        </svg>
                                        <svg
                                            className="mx-1 w-4 h-4 fill-current text-yellow-500"
                                            xmlns="http://www.w3.org/2000/svg"
                                            viewBox="0 0 20 20"
                                        >
                                            <path d="M10 15l-5.878 3.09 1.123-6.545L.489 6.91l6.572-.955L10 0l2.939 5.955 6.572.955-4.756 4.635 1.123 6.545z" />
                                        </svg>
                                        <svg
                                            className="mx-1 w-4 h-4 fill-current text-yellow-500"
                                            xmlns="http://www.w3.org/2000/svg"
                                            viewBox="0 0 20 20"
                                        >
                                            <path d="M10 15l-5.878 3.09 1.123-6.545L.489 6.91l6.572-.955L10 0l2.939 5.955 6.572.955-4.756 4.635 1.123 6.545z" />
                                        </svg>
                                    </div>
                                </div>
                                <p className="leading-relaxed mb-5 text-tiffany">
                                    5 star everytime. I've used Cloud Shipping
                                    quite a few times now and they've never let
                                    me down. Will continue to use them, no
                                    question. Big thumbs up.
                                </p>
                                <div className="inline-flex items-center">
                                    <img
                                        src={Teamzener}
                                        alt=""
                                        className="w-12 h-12 rounded-full flex-shrink-0 object-cover object-center"
                                    />
                                    <span className="flex-grow flex flex-col pl-3">
                                        <span className="title-font font-medium text-tiffany">
                                            Calum Hood
                                        </span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="py-8 px-4 lg:w-1/3">
                        <div className="h-full flex items-start">
                            <div className="flex-grow pl-6">
                                <h2 className="tracking-widest text-xs title-font font-medium text-tiffany-light mb-1">
                                    October 1, 2021
                                </h2>
                                <div className="flex justify-left items-left">
                                    <div className="flex items-left mt-2 mb-4">
                                        <svg
                                            className="mx-1 w-4 h-4 fill-current text-yellow-500"
                                            xmlns="http://www.w3.org/2000/svg"
                                            viewBox="0 0 20 20"
                                        >
                                            <path d="M10 15l-5.878 3.09 1.123-6.545L.489 6.91l6.572-.955L10 0l2.939 5.955 6.572.955-4.756 4.635 1.123 6.545z" />
                                        </svg>
                                        <svg
                                            className="mx-1 w-4 h-4 fill-current text-yellow-500"
                                            xmlns="http://www.w3.org/2000/svg"
                                            viewBox="0 0 20 20"
                                        >
                                            <path d="M10 15l-5.878 3.09 1.123-6.545L.489 6.91l6.572-.955L10 0l2.939 5.955 6.572.955-4.756 4.635 1.123 6.545z" />
                                        </svg>
                                        <svg
                                            className="mx-1 w-4 h-4 fill-current text-yellow-500"
                                            xmlns="http://www.w3.org/2000/svg"
                                            viewBox="0 0 20 20"
                                        >
                                            <path d="M10 15l-5.878 3.09 1.123-6.545L.489 6.91l6.572-.955L10 0l2.939 5.955 6.572.955-4.756 4.635 1.123 6.545z" />
                                        </svg>
                                        <svg
                                            className="mx-1 w-4 h-4 fill-current text-yellow-500"
                                            xmlns="http://www.w3.org/2000/svg"
                                            viewBox="0 0 20 20"
                                        >
                                            <path d="M10 15l-5.878 3.09 1.123-6.545L.489 6.91l6.572-.955L10 0l2.939 5.955 6.572.955-4.756 4.635 1.123 6.545z" />
                                        </svg>
                                        <svg
                                            className="mx-1 w-4 h-4 fill-current text-yellow-500"
                                            xmlns="http://www.w3.org/2000/svg"
                                            viewBox="0 0 20 20"
                                        >
                                            <path d="M10 15l-5.878 3.09 1.123-6.545L.489 6.91l6.572-.955L10 0l2.939 5.955 6.572.955-4.756 4.635 1.123 6.545z" />
                                        </svg>
                                    </div>
                                </div>
                                <p className="leading-relaxed mb-5 text-tiffany">
                                    Brilliant service, easy to use online. Great
                                    tracking and prompt delivery. Hands down!
                                </p>
                                <div className="inline-flex items-center">
                                    <img
                                        src={Teamklyde}
                                        alt=""
                                        className="w-12 h-12 rounded-full flex-shrink-0 object-cover object-center"
                                    />
                                    <span className="flex-grow flex flex-col pl-3">
                                        <span className="title-font font-medium text-tiffany">
                                            Tom Cullen
                                        </span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default Review;
