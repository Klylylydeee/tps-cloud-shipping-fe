// NPM Packages
import React from "react";

// Assets
import PWA3 from "src/Assets/pwa3.png";
import PWA4 from "src/Assets/pwa4.png";
import Logo from "src/Assets/logo.png";

const Partner = () => {
    return (
        <section className=" bg-tiffany-light">
            <div className="container px-5 py-24 mx-auto">
                <div className="flex flex-col text-center w-full mb-20">
                    <h1 className="text-5xl font-medium title-font mb-4 text-gray-900 tracking-widest">
                        PARTNERS
                    </h1>
                    <p className="lg:w-2/3 mx-auto leading-relaxed text-base">
                        Below are the brands we work with...
                    </p>
                </div>
                <div className="flex flex-wrap -m-4">
                    <div className="lg:w-1/4 md:w-1/2 p-4 w-full">
                        <div className="block relative h-48 rounded overflow-hidden">
                            <img
                                src={PWA3}
                                alt=""
                                className="object-contain h-48 w-full"
                            />
                        </div>
                    </div>
                    <div className="lg:w-1/4 md:w-1/2 p-4 w-full">
                        <div className="block relative h-48 rounded overflow-hidden">
                            <img
                                src={PWA3}
                                alt=""
                                className="object-contain h-48 w-full"
                            />
                        </div>
                    </div>
                    <div className="lg:w-1/4 md:w-1/2 p-4 w-full">
                        <div className="block relative h-48 rounded overflow-hidden">
                            <img
                                src={Logo}
                                alt=""
                                className="object-contain h-30 w-30 "
                            />
                        </div>
                    </div>
                    <div className="lg:w-1/4 md:w-1/2 p-4 w-full">
                        <div className="block relative h-48 rounded overflow-hidden">
                            <img
                                src={PWA3}
                                alt=""
                                className="object-contain h-48 w-full"
                            />
                        </div>
                    </div>
                    <div className="lg:w-1/4 md:w-1/2 p-4 w-full">
                        <div className="block relative h-48 rounded overflow-hidden">
                            <img
                                src={PWA3}
                                alt=""
                                className="object-contain h-48 w-full"
                            />
                        </div>
                    </div>
                    <div className="lg:w-1/4 md:w-1/2 p-4 w-full">
                        <div className="block relative h-48 rounded overflow-hidden">
                            <img
                                src={PWA3}
                                alt=""
                                className="object-contain h-48 w-full"
                            />
                        </div>
                    </div>
                    <div className="lg:w-1/4 md:w-1/2 p-4 w-full">
                        <div className="block relative h-48 rounded overflow-hidden">
                            <img
                                src={PWA4}
                                alt=""
                                className="object-contain h-48 w-full"
                            />
                        </div>
                    </div>
                    <div className="lg:w-1/4 md:w-1/2 p-4 w-full">
                        <div className="block relative h-48 rounded overflow-hidden">
                            <img
                                src={PWA3}
                                alt=""
                                className="object-contain h-48 w-full"
                            />
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default Partner;
