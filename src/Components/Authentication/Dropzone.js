export const thumbsContainer = {
    display: "flex",
    flexDirection: "row",
    flexWrap: "wrap",
    marginTop: 16,
};

export const thumb = {
    display: "inline-flex",
    borderRadius: 2,
    border: "1px solid #eaeaea",
    marginBottom: 8,
    marginRight: 8,
    width: "100px",
    height: "75px",
    padding: 4,
    boxSizing: "border-box",
};

export const thumbInner = {
    display: "flex",
    minWidth: 0,
    overflow: "hidden",
};

export const img = {
    display: "block",
    width: "auto",
    height: "100%",
};