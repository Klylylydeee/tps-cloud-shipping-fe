// NPM Packages
import React, { useState, useContext, useEffect } from "react";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router";

// Moduled Functions
import { signIn } from "src/State/UserInformation/UserData";
import { axiosAPI } from "src/Middleware/Axios";
import { notify } from "src/Services/Toaster";
import { ContextAPI } from "src/Middleware/Context";

// Assets
import cloudShippingLogo from "src/Assets/cloudlogo.png";
import backgroundImage from "src/Assets/sidebar-background.jpg";

const SignIn = () => {
    // Component Initial Variables
    const dispatch = useDispatch();                                             // Use Reducer Actions
    const Auth = useContext(ContextAPI);                                        // Context API Variable from Parent to check current Authorization Status
    const History = useHistory();                                               // Navigate to different Routes of the Router
    const [ username, setUsername ] = useState("");                     // Username input field value
    const [ password, setPassword ] = useState("");                     // Password input field value
    const [ passwordParam, setPasswordParam ] = useState("");                     // Password input field value

    // Login Request to be submitted to the API as a request body
    const signInRequest = async (formTarget) => {
        formTarget.preventDefault();
        try {
            console.log(password)
            const loginResponse = await axiosAPI.post('authentication/sign-in', {
                username: username,
                password: password
            })
            dispatch(signIn({
                id: loginResponse.data.status.id,
                first_name: loginResponse.data.status.first_name.charAt(0).toUpperCase() + loginResponse.data.status.first_name.slice(1),
                last_name: loginResponse.data.status.last_name.charAt(0).toUpperCase() + loginResponse.data.status.last_name.slice(1),
                username: loginResponse.data.status.username,
                user_picture: loginResponse.data.status.user_picture,
                address: loginResponse.data.status.address,
                email: loginResponse.data.status.email,
                phone_number: loginResponse.data.status.phone_number,
                role: loginResponse.data.status.role
            }))
            localStorage.setItem("Authorization", loginResponse.data.payload)
            Auth.setAuth(true)
            notify(`Welcome ${loginResponse.data.status.first_name.charAt(0).toUpperCase() + loginResponse.data.status.first_name.slice(1)} ${loginResponse.data.status.last_name.charAt(0).toUpperCase() + loginResponse.data.status.last_name.slice(1)}`, "success")  
        } catch (err) {
            err.response ? notify(err.response.data.message, "success") : notify(err.message, "success")  
        }
    }

    useEffect(() => {
        let value = password.split("").map((currentCharacter) => {
            return "*"
        }).join("");
        // eslint-disable-next-line 
        setPasswordParam(value);
    }, [password])
 
    return (
        <div className="min-w-min min-h-screen flex">
            <div className="min-h-screen lg:w-2/5 w-full p-10 flex flex-col justify-center items-center">
                <label className="self-start text-2xl pb-2 font-semibold" htmlFor="emailinput">
                    Welcome Back!
                </label>
                <label className="self-start text-base pb-10" htmlFor="emailinput">
                    To keep connected with us please sign in with your personal credentials
                </label>
                <form className="w-full mx-auto" onSubmit={signInRequest}>
                    <label className="block text-sm mb-1" htmlFor="emailinput">
                        Username
                    </label>
                    <input
                        className="w-full  py-2 border-b border-gray-300 focus:outline-none focus:border-indigo-500"
                        type=""
                        placeholder=""
                        id="emailinput"
                        value={username}
                        onChange={(e)=>{
                            setUsername(e.target.value)
                        }}
                        required
                    />
                    <label className="block text-sm mb-1 mt-4" htmlFor="passwordinput">
                        Password
                    </label>
                    <input
                        className=" w-full py-2 border-b border-gray-300 focus:outline-none focus:border-indigo-500"
                        type=""
                        placeholder="••••••••"
                        id="passwordinput"
                        value={passwordParam}
                        onChange={(e)=>{
                            e.target.value.charAt(e.target.value.length-1).length === 0 &&
                                setPassword((existingString) => {
                                    return ""
                                })
                            e.target.value.charAt(e.target.value.length-1) === "*" || e.target.value.charAt(e.target.value.length-1) === "" ?
                                setPassword((existingString) => {
                                    return existingString.slice(0, -1)
                                })
                            :
                            setPassword((existingString) => {
                                return `${existingString}${e.target.value.charAt(e.target.value.length-1)}`
                            })
                        }}
                        required
                    />
                    <input
                        type="submit"
                        className="bg-tiffany-30 text-gray-100 p-2 my-4 w-full rounded-full tracking-wide
                        font-semibold font-display focus:outline-none focus:shadow-outline hover:bg-tiffany
                        shadow-lg"
                        value="Login"
                    />
                </form>
                <p className="my-4 text-xs">
                    Don't have an Account yet? <span className="text-tiffany cursor-pointer" onClick={() => { History.push("/sign-up") }}>Sign Up</span>
                </p>
            </div>
            <div className="min-h-screen w-3/5 rgba(1,1,1,0.5) lg:w-3/5 lg:flex hidden flex-col justify-center items-center relative"
            style={{ 
                maxHeight: "100vh",
                backgroundImage: `url(${backgroundImage})`,
                backgroundPosition: "60% 35%",
                backgroundColor: "rgb(10, 186, 181)",
                backgroundBlendMode: "multiply"
            }}>
            <div className="block absolute min-h-full" style={{ minWidth: "100%", background: 'linear-gradient(360deg,  rgba(75, 206, 243, 0.65) 0%, rgba(75, 206, 243, 1) 80%)' }}/>
                <img src={cloudShippingLogo} alt="" style={{ height: "10rem", width: "20rem" }} className="transform h-64 transition duration-500 hover:scale-110 cursor-pointer" onClick={() => { History.push("/") }}/>
                <p class="text-white text-2xl font-semibold my-4 w-4/5 text-center" style={{ zIndex: 999 }}>
                    Transforming the Shipping and Business Industry for the Next Generation
                </p>
            </div>
        </div>
    );
};

export default SignIn;
